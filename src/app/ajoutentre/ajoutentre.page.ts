import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-ajoutentre',
  templateUrl: './ajoutentre.page.html',
  styleUrls: ['./ajoutentre.page.scss'],
})
export class AjoutentrePage implements OnInit {

  constructor(private route:Router, public  publicService: PublicService, private router:Router ) { }
  ngOnInit() {
  }
  onAjout(entrer) {
    var id = localStorage.getItem('userId');
       this.publicService.ajoutentree({
         Produit: entrer.Designation,
         Prix_Unitaire:entrer.Prix,
         Quantite:entrer.Quantite,
       })
       .then(data =>{
         console.log(data);
         if(data){
          this.router.navigateByUrl('');
          console.log('ajout réussi')
        }else{
          this.router.navigateByUrl('ajoutvente');
          console.log('échec') 
        }
      }, err => {
         console.log(err);
      });
      console.log(entrer);
     
   }
  

}
