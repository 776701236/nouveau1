import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-ajoutsorties',
  templateUrl: './ajoutsorties.page.html',
  styleUrls: ['./ajoutsorties.page.scss'],
})
export class AjoutsortiesPage implements OnInit {

  constructor(private route:Router, public  publicService: PublicService, private router:Router) { }

  ngOnInit() {
  }
  onAjout(sorti) {
    var id = localStorage.getItem('userId');
       this.publicService.ajoutsortie({
         Produit: sorti.Produit,
         Prix_Unitaire:sorti.Prix_Unitaire,
         Quantite:sorti.Quantite,
       })
       .then(data =>{
         console.log(data);
         if(data){
          this.router.navigateByUrl('');
          console.log('ajout réussi')
        }else{
          this.router.navigateByUrl('ajoutvente');
          console.log('échec') 
        }
      }, err => {
         console.log(err);
      });
      console.log(sorti);
     
   }
  

}

