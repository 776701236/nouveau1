import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajoutdepense',
  templateUrl: './ajoutdepense.page.html',
  styleUrls: ['./ajoutdepense.page.scss'],
})
export class AjoutdepensePage implements OnInit {

 constructor(private route:Router, public  publicService: PublicService ) { }

  ngOnInit() {
  }

 onAjout(depense) {
   var id = localStorage.getItem('userId');
     this.publicService.ajoutdepense({
       Designation: depense.Designation,
       Prix: depense.Prix,
       id_userd:id,
     })
     .then(data =>{
       console.log(data);
    }, err => {
       console.log(err);
    });
    console.log(depense);
   
 }

}
