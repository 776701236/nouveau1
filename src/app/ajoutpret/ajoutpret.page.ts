import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-ajoutpret',
  templateUrl: './ajoutpret.page.html',
  styleUrls: ['./ajoutpret.page.scss'],
})
export class AjoutpretPage implements OnInit {
  

  constructor(private route:Router, public  publicService: PublicService, private router:Router ) { }

  ngOnInit() {
  }

    
onAjout(pret) {
 console.log(pret);
 if(pret.prete == "Remboursement"){
  var id = localStorage.getItem('userId');
  this.publicService.ajoutpret({
  
   Designation:pret.Libelle,
   Montant: - pret.Montant,
   Client:pret.Nom_Client,
   Telephone: pret.Phone,
   id_userp:id,
  })
  .then(data =>{
    console.log(data);
    if(data){
     this.router.navigateByUrl('/menu/acceuil/menu/acceuil/vente');
     console.log('pret')
   }else{
     this.router.navigateByUrl('ajoutvente');
     console.log('échec') 
   }
 }, err => {
    console.log(err);
 });
  }else{
    var id = localStorage.getItem('userId');
  this.publicService.ajoutpret({
  
   Designation:pret.Libelle,
   Montant:-pret.Montant,
   Client:pret.Nom_Client,
   Telephone: pret.Phone,
   id_userp:id,
  })
  .then(data =>{
    console.log(data);
    if(data){
     this.router.navigateByUrl('/menu/acceuil/menu/acceuil/vente');
     console.log('ajout réussi')
   }else{
     this.router.navigateByUrl('ajoutvente');
     console.log('échec') 
   }
 }, err => {
    console.log(err);
 });
  }
  
 }
 

}
