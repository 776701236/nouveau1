import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistSortiPage } from './hist-sorti.page';



const routes: Routes = [
  {
    path: 'menu/hist-sorti',
    component: HistSortiPage,
    children: [
      {
        path: 'journaliers1',
        children: [
          {
            path: '',
            loadChildren: '../journaliers1/journaliers1.module#Journaliers1PageModule'
          }
        ]
              },
      {
        path: 'mois1',
        children: [
          {
            path: '',
            loadChildren: '../mois1/mois1.module#Mois1PageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'menu/hist-sorti/journaliers1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu/hist-sorti/journaliers1',
    pathMatch: 'full'
}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistSortiPage]
})
export class HistSortiPageModule {}
