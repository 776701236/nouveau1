import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InventairesPage } from './inventaires.page';

const routes: Routes = [
  {
    path: 'menu/inventaires',
    component: InventairesPage,
    children: [
      {
        path: 'journalier',
        children: [
          {
            path: '',
            loadChildren: '../journalier/journalier.module#JournalierPageModule'
          }
        ]
      },
      {
        path: 'mensuel',
        children: [
          {
            path: '',
            loadChildren: '../mensuel/mensuel.module#MensuelPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: 'menu/inventaires/journalier',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu/inventaires/journalier',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InventairesPage]
})
export class InventairesPageModule {}
