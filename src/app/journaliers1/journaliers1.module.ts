import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Journaliers1Page } from './journaliers1.page';

const routes: Routes = [
  {
    path: '',
    component: Journaliers1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Journaliers1Page]
})
export class Journaliers1PageModule {}
