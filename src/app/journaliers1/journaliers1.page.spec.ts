import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Journaliers1Page } from './journaliers1.page';

describe('Journaliers1Page', () => {
  let component: Journaliers1Page;
  let fixture: ComponentFixture<Journaliers1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Journaliers1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Journaliers1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
