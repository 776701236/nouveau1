import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mois1Page } from './mois1.page';

describe('Mois1Page', () => {
  let component: Mois1Page;
  let fixture: ComponentFixture<Mois1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mois1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mois1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
