
import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authentication.service'
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  result : any ;
  isLoginError : boolean = false;

  constructor(private authService:AuthenticateService, private router:Router){ 

  }
  
  ngOnInit() {}


  onLogin(value) { 
    this.authService.login({
      phone:value.phone,
      password:value.password
    }).then(data  =>{
      this.result = JSON.parse(data.data);
      console.log('Joyeux Noel ', this.result.nom_complet);
      // recuperation de l'id
      localStorage.setItem('userId', this.result.id)
      localStorage.setItem('token', this.result.token.token)

      if(data){
        this.router.navigateByUrl('/menu/acceuil');
        console.log('success')
      }else{
        this.router.navigateByUrl('login');
        console.log(value.password, 'login incorrect') 
      }
    }, 
    (err : HttpErrorResponse)=>{
      this.isLoginError = true;
      console.log(err);
    });
  }
 
  
  public logo = {
    logo : "assets/images/fewnu.jpg",

  }

  public logo1 = { 
    logo : "assets/images/Phone.png",

  }

  public logo2 = {
    logo : "assets/images/security.png",

  }
  
}
