import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistEntrerPage } from './hist-entrer.page';

describe('HistEntrerPage', () => {
  let component: HistEntrerPage;
  let fixture: ComponentFixture<HistEntrerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistEntrerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistEntrerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
