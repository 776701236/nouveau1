import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HebdomadairePage } from './hebdomadaire.page';

describe('HebdomadairePage', () => {
  let component: HebdomadairePage;
  let fixture: ComponentFixture<HebdomadairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HebdomadairePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HebdomadairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
