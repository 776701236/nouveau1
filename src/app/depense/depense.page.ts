import { Component, OnInit } from '@angular/core';
import { PublicService } from '../services/public.service';

@Component({
  selector: 'app-depense',
  templateUrl: './depense.page.html',
  styleUrls: ['./depense.page.scss'],
})
export class DepensePage implements OnInit {

  constructor(private publc:PublicService) { }

  result : any ;

  ngOnInit() {
    this.onLoadDepense();
  }

  onLoadDepense() {
    this.publc.getDepense()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
    },err=>{
      console.log(err); 
    });
  }

}
