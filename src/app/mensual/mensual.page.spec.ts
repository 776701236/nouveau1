import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensualPage } from './mensual.page';

describe('MensualPage', () => {
  let component: MensualPage;
  let fixture: ComponentFixture<MensualPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensualPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensualPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
