import { Injectable } from '@angular/core';
import {rootUrl} from "../../app/apiurls/serverurls.js";
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private http:HTTP) { }

  getVente() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/vente/"+id+"/", {}, {});  
  }

  getDepense() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/depense/"+id+"/", {}, {});  
  }

  getPret() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl+"/api/pret/"+id+"/", {}, {});  
  }

  ajoutvente(vente){
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl+"/api/vente/"+id+"/", vente, {});
  }

  ajoutdepense(depense){
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl+"/api/depense/"+id+"/", depense, {});
  }
  ajoutpret(pret){
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl+"/api/pret/"+id+"/", pret, {});
  }

  ajoutentree(entre){
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl+"/api/Entree/"+id+"/", entre, {});
  }

  ajoutsortie(sorti){
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl+"/api/Sortie/"+id+"/", sorti, {});
  }

}
